/*Теоритичні питання та відповіді

1. Опишіть своїми словами, що таке Document Object Model (DOM).
DOM - це інтерфейс, який дозволяє програмам (наприклад, JavaScript) змінювати та маніпулювати вмістом веб-сторінки.

2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
Використання innerHTML може призвести до впливу на внутрішню структуру HTML-елемента, тоді як innerText працює тільки з текстом, не змінюючи HTML-структуру.

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий? 
Можна звернутися до елемента сторінки за допомогою методу document.getElementById(). Він дозволяє знайти елемент за його унікальним ідентифікатором (id). Інший спосіб - використовувати document.querySelector(), 
де можна звертатися до елементів за класами, тегами або іншими CSS-селекторами. Вибір найкращого способу залежить від ситуації. Якщо у елемента є унікальний ідентифікатор, getElementById() - кращий. 
Але, якщо потрібно більше гнучкості у виборі елементів, то querySelector() допоможе більше.
*/

//ОСНОВНЕ ЗАВДАННЯ З НАПИСАННЯ КОДУ
//Завдання 1: Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let paragraphs = document.querySelectorAll("p");
paragraphs.forEach((elem) => {
  elem.style.backgroundColor = "#ff0000";
});

//Завдання 2: Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let element = document.getElementById("optionsList");
console.log(element);
console.log(element.parentElement);
if (element.childElementCount > 0) {
  const nodes = element.childNodes;
  let text = "";
  for (let i = 0; i < nodes.length; i++) {
    text +=
      "Child node name: " +
      nodes[i].nodeName +
      " Child node type: " +
      nodes[i].nodeType +
      "\n";
  }
  console.log(text);
}

//Завдання 3: Встановіть в якості контента елемента з id="testParagraph" наступний параграф - This is a paragraph
document.querySelector("#testParagraph").innerHTML =
  "<p>This is a paragraph</p>";

//Завдання 4: Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.
let elem = document.querySelectorAll(".main-header li");
console.log(elem);
elem.forEach((item) => {
  item.classList.add("nav-item");
});

// Завдання 5: Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let sectionTitles = document.querySelectorAll(".section-title");
sectionTitles.forEach((item) => {
  item.classList.remove("section-title");
});
